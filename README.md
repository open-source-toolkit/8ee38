# NAS群辉DS918+安装资源文件

## 简介

本仓库提供了一个用于NAS群辉DS918+安装的资源文件，包括引导文件和系统安装包。这些文件可以帮助用户快速完成DS918+的安装和配置。

## 文件列表

- **DS918+_引导文件.img**: 用于引导安装的镜像文件。
- **DS918+_23824.pat**: 群辉DS918+的系统安装包。

## 使用说明

1. **下载文件**: 请从本仓库下载所需的`DS918+_引导文件.img`和`DS918+_23824.pat`文件。

2. **准备安装介质**: 将`DS918+_引导文件.img`写入U盘或SD卡，作为引导介质。

3. **安装系统**: 将准备好的引导介质插入DS918+，启动设备并按照提示进行系统安装。在安装过程中，选择下载的`DS918+_23824.pat`文件作为系统安装包。

4. **完成安装**: 按照屏幕提示完成系统安装和初始配置。

## 注意事项

- 请确保下载的文件与您的DS918+设备型号和版本匹配。
- 在安装过程中，请确保网络连接稳定，以便顺利下载和安装系统文件。

## 贡献

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交Issue或Pull Request。

## 许可证

本仓库中的文件遵循相应的开源许可证，具体请参考文件中的许可证声明。

---

希望这些资源文件能帮助您顺利完成NAS群辉DS918+的安装！